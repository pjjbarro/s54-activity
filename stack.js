let collection = ["a","b","c","d","e"];

function addToStack(element){

    collection.push(element);
    return collection;

};

addToStack("Chocolate");

function removeFromStack(){

    collection.pop();
    return collection;

};

removeFromStack();

function peek(){

    return collection[collection.length-1]

}

peek();

function getLength(){

    return collection.length

}

getLength()
